﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace wlkParser.Model
{
    public class DailySummary2
    {
        public byte dataType = 3;
        public byte reserved;   // this will cause the rest of the fields to start on an even address

        // this field is not used now.
        public UInt16 todaysWeather; // bitmapped weather conditions (Fog, T-Storm, hurricane, etc)

        public short numWindPackets;      // # of valid packets containing wind data, 
                                          // this is used to indicate reception quality
        public short hiSolar;             // Watts per meter squared
        public short dailySolarEnergy;    // 1/10'th Ly
        public short minSunlight;         // number of accumulated minutes where the avg solar rad > 150
        public short dailyETTotal;        // 1/1000'th of an inch
        public short hiHeat, lowHeat;     // tenths of a degree F
        public short avgHeat;             // tenths of a degree F
        public short hiTHSW, lowTHSW;     // tenths of a degree F
        public short hiTHW, lowTHW;       // tenths of a degree F

        // integrated Heating Degree Days (65F threshold)
        // tenths of a degree F - Day
        public short integratedHeatDD65;


        // Wet bulb values are not calculated
        public short hiWetBulb, lowWetBulb; // tenths of a degree F
        public short avgWetBulb;          // tenths of a degree F

        // space for 16 direction bins 
        // (Used to calculate monthly dominent Dir)
        public byte[] dirBins = new byte[24];


        public byte[] timeValues = new byte[15];       // space for 10 time values (see below)

        public short integratedCoolDD65;  // integrated Cooling Degree Days (65F threshold)
                                          // tenths of a degree F - Day
        public byte[] reserved2 = new byte[11];



        public void Read(BinaryReader reader)
        {
            dataType = reader.ReadByte();
            reserved = reader.ReadByte();

            // Validar el registro según el típo de dato
            if (dataType != 3) throw new Exception("WLK Parse: El registro DailySummary2 no es del tipo correcto");
        }

    }
}
