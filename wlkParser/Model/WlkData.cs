﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using wlkParser.Model;

namespace wlkPaser
{

    /// <summary>
    /// Datos Diarios y Horarios de una estación Davis para un período Año-Mes
    /// Estructura del archivo
    /// Header
    ///     Cantidad de registros
    ///     
    /// Repetido por la cantidad de días informados
    /// ---> Summary1
    /// ---> Summary2
    /// -------> Mediciones horarias cada 30 minutos: WeatherRecord
    /// ---> Summary1
    /// ---> Summary2
    /// -------> Mediciones horarias cada 30 minutos: WeatherRecord
    /// ---> Summary1
    /// ---> Summary2
    /// -------> Mediciones horarias cada 30 minutos: WeatherRecord
    /// 
    /// 
    /// </summary>
    public class WlkData
    {
        // Identifica la estación 
        public int IdEstacion { get; set; }
        // Nombre de la estación o localidad
        public string Estacion { get; set; }
        // Los datos corresponden a un año-mes
        public int Anio { get; set; }
        // Mes de los datos del archivo
        public int Mes { get; set; }


        public DateTime InicioMes { get { return new DateTime(Anio, Mes, 1); } }
        public int DiasEnMes { get { return DateTime.DaysInMonth(Anio, Mes); } }


        #region Constructor
        public WlkData()
        {
            // TODO: Se debe inicializar según el archivo
            Anio = 2018;
            Mes = 3;
            // 

            Header = new HeaderBlock();
            DatosDia = new DatosDia[32];
            // Inicializar las fechas
            for (int d = 1; d <= DiasEnMes; d++)
            {
                DatosDia[d] = new DatosDia(InicioMes.AddDays(d - 1));
                DatosDia[d].dailySummary1 = new DailySummary1();
                DatosDia[d].dailySummary2 = new DailySummary2();
            }
        }
        #endregion

        private HeaderBlock Header { get; set; }
        public DatosDia[] DatosDia;
       

        /// <summary>
        /// Lee desde un BinaryReader el encabezado del archivo
        /// </summary>
        /// <param name="reader"></param>
        public void Read(BinaryReader reader)
        {
            Header.Read(reader);

            for (int dia = 1; Header.DayIndex[dia].recordsInDay > 0; dia++)
            {
                int offset = GetRecordOffset(Header.DayIndex[dia].startPos);
                var datosdia = DatosDia[dia];
                reader.BaseStream.Seek((long)offset, 0);
                datosdia.dailySummary1.Read(reader);

                // Summary 2
                offset += 88;
                reader.BaseStream.Seek((long)offset, 0);
                datosdia.dailySummary2.Read(reader);

                // WeatherDataRecord
                offset += 88;
                ReadWeatherRecords(datosdia, Header.DayIndex[dia].recordsInDay, offset, reader);
            }
        }
  
        /// <summary>
        /// Lee los registros de datos horarios de cada día; están intercalados entre los dos resumenes diarios
        /// </summary>
        /// <param name="datosDia"></param>
        /// <param name="records"></param>
        /// <param name="offset"></param>
        /// <param name="reader"></param>
        private void ReadWeatherRecords(DatosDia datosDia, int records, int offset, BinaryReader reader)
        {
            for (int i = 1; i< records - 1; i++)
            {
                var wdr = new WeatherDataRecord();
                datosDia.weaterData[i] = wdr;
                wdr.Read(reader, offset);
                offset += 88;

                DatoHorario dh = new DatoHorario(datosDia.Fecha, wdr);
                datosDia.DatosHorarios.Add(dh);

            }
        }

        public void PrintData()
        {
            for (int d = 1; d <= DiasEnMes && Header.DayIndex[d].recordsInDay > 0; d++)
            {
                var datoDia = DatosDia[d];
                Console.WriteLine($"Dia: {datoDia.Fecha.ToShortDateString()} - Temp [{datoDia.TempMin} - {datoDia.TempMax}]°C    Sens.Term. [{datoDia.SensacionTermicaMin} - {datoDia.SensacionTermicaMax}] °C      Humedad: [{datoDia.HumedadMin} - {datoDia.HumedadMax}] Presión:[ {datoDia.PresionMin} - {datoDia.PresionMax}]HPa\tRad.Solar {datoDia.RadiacionSolar}w/m2");
                for (int i = 0; i < datoDia.DatosHorarios.Count; i++)
                {
                    var dh = datoDia.DatosHorarios[i];
                    Console.WriteLine($"   -------Hora: {dh.FechaHora.ToString("HH:mm")}\tTemp  {dh.Temperatura}°C\tViento: {dh.VelViento} km/h\t Dir.Viento: {dh.DirViento}\tPresión: {dh.Presion}hPa\tRadiación [{dh.RadiacionSolar} - {dh.MaxRadiacionSolar}]"  );
                }
            }

        }

        
        /// <summary>
        /// Posicion inicial del registro 
        /// 
        /// </summary>
        /// <param name="recNum"></param>
        /// <returns></returns>
        private int GetRecordOffset(int recNum)
        {
            return 16 + 4 + 6 * 32 + recNum * 88;
        }

        private class HeaderBlock
        {
            public char[] IdCode = new char[16]; // = {'W', 'D', 'A', 'T', '5', '.', '0', 0, 0, 0, 0, 0, 0, 0, 5, 0}
            public int TotalRecords { get; set; }
            // index records for each day. Index 0 is not used
            // (i.e. the 1'st is at index 1, not index 0)
            public DayIndex[] DayIndex = new DayIndex[32];

            public void Read(BinaryReader reader)
            {
                IdCode = reader.ReadChars(16);
                ValidarCabecera();
                TotalRecords = reader.ReadInt32();

                for (int i = 0; i < 32; i++)
                {
                    DayIndex[i] = new DayIndex();
                    DayIndex[i].recordsInDay = reader.ReadInt16();
                    DayIndex[i].startPos = reader.ReadInt32();
                }
            }


            public void ValidarCabecera()
            {
                const string firma = "WDAT5.3";
                string cabecera = new string(IdCode).Substring(0,7); 
                if (string.Compare(firma, cabecera) != 0) throw new Exception($"La cabecera del archivo {cabecera} difiere de la firma: {firma}");
            }

        };

        /// <summary>
        /// Estructura auxiliar del arvhivo WLK
        /// </summary>
        private class DayIndex
        {
            public Int16 recordsInDay;  // includes any daily summary records
            public int startPos;    // The index (starting at 0) of the first daily summary record
        }

    }




   
    
   

}
