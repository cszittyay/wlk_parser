﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace wlkParser.Model
{
    public class DatoHorario
    {
        public DatoHorario(DateTime fecha, WeatherDataRecord aWDR)
        {
            wdr = aWDR;
            FechaHora = fecha.AddMinutes(Convert.ToDouble(wdr.packedTime));
        }

        public DateTime FechaHora { get; set; }
        public double Temperatura { get { return Math.Round((Convert.ToDouble(wdr.outsideTemp) / 10.0 - 32.0) * 5.0 / 9.0, 1); } }
        public double VelViento { get { return Math.Round(Convert.ToDouble(wdr.windSpeed) * 0.1852, 1); } }
        public string DirViento { get { return wdr.windDirection == 255 ? "" : RosaVientos[(int)wdr.windDirection]; } }
        public int Lluvia { get { return wdr.hiRainRate; } }
        public double Presion { get { return Math.Round(Convert.ToDouble(wdr.barometer) * 0.0338639, 0); } }
        public int RadiacionSolar { get { return Convert.ToInt32(wdr.solarRad); } }
        public int MaxRadiacionSolar { get {return Convert.ToInt32(wdr.hisolarRad); } }

        private WeatherDataRecord wdr { get; set; }

        private string[] RosaVientos = new string[] { "N", "NNE", "NE", "ENE", "E", "ESE", "SE", "SSE", "S", "SSW", "SW", "WSW", "W", "WNW", "NW", "NNW" };
    }
}
