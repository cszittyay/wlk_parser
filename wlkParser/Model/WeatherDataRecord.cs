﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace wlkParser.Model
{
    /// <summary>
    /// Dato horario: cada 30 minutos un nuevo registro.
    /// </summary>
    public class WeatherDataRecord
    {
        public byte dataType = 1;
        public byte archiveInterval;      // number of minutes in the archive
                                          // see below for more detatils about these next two fields)
        public byte iconFlags;            // Icon associated with this record, plus Edit flags
        public byte moreFlags;            // Tx Id, etc.

        public short packedTime;          // minutes past midnight of the end of the archive period
        public short outsideTemp;         // tenths of a degree F
        public short hiOutsideTemp;       // tenths of a degree F
        public short lowOutsideTemp;      // tenths of a degree F
        public short insideTemp;          // tenths of a degree F
        public short barometer;           // thousanths of an inch Hg
        public short outsideHum;          // tenths of a percent
        public short insideHum;           // tenths of a percent
        public short rain;                // number of clicks + rain collector type code
        public short hiRainRate;          // clicks per hour
        public short windSpeed;           // tenths of an MPH
        public short hiWindSpeed;         // tenths of an MPH
        public byte windDirection;        // direction code (0-15, 255)
        public byte hiWindDirection;      // direction code (0-15, 255)
        public short numWindSamples;      // number of valid ISS packets containing wind data
                                          // this is a good indication of reception 
        public short solarRad, hisolarRad;// Watts per meter squared 
        public byte UV, hiUV;            // tenth of a UV Index

        public byte[] leafTemp = new byte[4];          // (whole degrees F) + 90

        public short[] newSensors = new short[7];       // reserved for future use
        public byte forecast;            // forecast code during the archive interval

        public byte ET;                  // in thousanths of an inch

        public byte[] soilTemp = new byte[6];          // (whole degrees F) + 90
        public byte[] soilMoisture = new byte[6];      // centibars of dryness
        public byte[] leafWetness = new byte[4];       // Leaf Wetness code (0-15, 255)
        public byte[] extraTemp = new byte[7];         // (whole degrees F) + 90
        public byte[] extraHum = new byte[7];          // whole percent


        /// <summary>
        ///  Lee los registros horarios: uno cada 30 minutos.
        /// </summary>
        /// <param name="reader"></param>
        /// <param name="recordsInDay"></param>
        /// <param name="offset"></param>
        public void Read(BinaryReader reader,  int offset)
        {
                reader.BaseStream.Seek((long)offset, 0);
                dataType = reader.ReadByte();
                archiveInterval = reader.ReadByte();
                iconFlags = reader.ReadByte();
                moreFlags = reader.ReadByte();
                // Nro de minutos después de medianoche.
                packedTime = reader.ReadInt16();
                outsideTemp = reader.ReadInt16();
                hiOutsideTemp = reader.ReadInt16();
                lowOutsideTemp = reader.ReadInt16();
                insideTemp = reader.ReadInt16();
                barometer = reader.ReadInt16();
                outsideHum = reader.ReadInt16();
                insideHum = reader.ReadInt16();
                rain = reader.ReadInt16();
                hiRainRate = reader.ReadInt16();
                windSpeed = reader.ReadInt16();
                hiWindSpeed = reader.ReadInt16();
                windDirection = reader.ReadByte();
                hiWindDirection = reader.ReadByte();
                numWindSamples = reader.ReadInt16();
                solarRad = reader.ReadInt16();
                hisolarRad = reader.ReadInt16();
                // el resto lo ignoramos porque no se usa.

         
        }

    };

}
