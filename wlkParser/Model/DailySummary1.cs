﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace wlkParser.Model
{
    public class DailySummary1
    {
        public byte dataType;
        public byte reserved;    // this will cause the rest of the fields to start on an even address

        public short dataSpan;   // total # of minutes accounted for by physical records for this day
        public short hiOutTemp, lowOutTemp; // tenths of a degree F
        public short hiInTemp, lowInTemp;   // tenths of a degree F
        public short avgOutTemp, avgInTemp; // tenths of a degree F (integrated over the day)
        public short hiChill, lowChill;     // tenths of a degree F
        public short hiDew, lowDew;         // tenths of a degree F
        public short avgChill, avgDew;      // tenths of a degree F 
        public short hiOutHum, lowOutHum;   // tenths of a percent
        public short hiInHum, lowInHum;     // tenths of a percent
        public short avgOutHum;             // tenths of a percent
        public short hiBar, lowBar;         // thousanths of an inch Hg
        public short avgBar;                // thousanths of an inch Hg
        public short hiSpeed, avgSpeed;     // tenths of an MPH
        public short dailyWindRunTotal;     // 1/10'th of an mile 
        public short hi10MinSpeed;          // the highest average wind speed record 
        public byte dirHiSpeed, hi10MinDir;        // direction code (0-15, 255)
        public short dailyRainTotal;        // 1/1000'th of an inch
        public short hiRainRate;            // 1/100'th inch/hr ???
        public short dailyUVDose;           // 1/10'th of a standard MED
        public byte hiUV;                  // tenth of a UV Index
        public byte[] timeValues = new byte[27];         // space for 18 time values (see below)


        /// <summary>
        ///  Leer el resumen diario
        /// </summary>
        /// <param name="reader"></param>
        public void Read(BinaryReader reader)
        {
            dataType = reader.ReadByte();
            reserved = reader.ReadByte();
            dataSpan = reader.ReadInt16();
            hiOutTemp = reader.ReadInt16();
            lowOutTemp = reader.ReadInt16();
            hiInTemp = reader.ReadInt16();
            lowInTemp = reader.ReadInt16();
            avgOutTemp = reader.ReadInt16();
            avgInTemp = reader.ReadInt16();
            hiChill = reader.ReadInt16();
            lowChill = reader.ReadInt16();
            hiDew = reader.ReadInt16();
            lowDew = reader.ReadInt16();
            avgChill = reader.ReadInt16();
            avgDew = reader.ReadInt16();
            hiOutHum = reader.ReadInt16();
            lowOutHum = reader.ReadInt16();
            hiInHum = reader.ReadInt16();
            lowInHum = reader.ReadInt16();
            avgOutHum = reader.ReadInt16();
            hiBar = reader.ReadInt16();
            lowBar = reader.ReadInt16();
            avgBar = reader.ReadInt16();
            hiSpeed = reader.ReadInt16();
            avgSpeed = reader.ReadInt16();
            dailyWindRunTotal = reader.ReadInt16();
            hi10MinSpeed = reader.ReadInt16();
            dirHiSpeed = reader.ReadByte();
            hi10MinDir = reader.ReadByte();
            dailyRainTotal = reader.ReadInt16();
            hiRainRate = reader.ReadInt16();
            dailyUVDose = reader.ReadInt16();
            hiUV = reader.ReadByte();
            timeValues = reader.ReadBytes(27);

            // Validar el registro según el típo de dato
            if (dataType != 2) throw new Exception("WLK Parse: El registro DailySummary1 no es del tipo correcto");
    
        }
    }
}
    
