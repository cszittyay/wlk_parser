﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace wlkParser.Model
{
    /// <summary>
    /// Los
    /// </summary>
    public class DatosDia
    {

        public DatosDia(DateTime fecha)
        {
            this.Fecha = fecha;
            DatosHorarios = new List<DatoHorario>();
        }

        

        public DateTime Fecha { get; set; }
        public DailySummary1 dailySummary1;
        public DailySummary2 dailySummary2;
        public WeatherDataRecord[] weaterData = new WeatherDataRecord[50];

        public List<DatoHorario> DatosHorarios { get; set; }

        #region Datos Diarios: Siempre son datos del exterior
        // Tempertaura máxima exterior 
        public double TempMax { get { return Fahr2Cels(dailySummary1.hiOutTemp); } }
        // Tempertaura minima exterior 
        public double TempMin { get { return Fahr2Cels(dailySummary1.lowOutTemp); } }
        // Sensacion termica Max
        public double SensacionTermicaMax { get { return Fahr2Cels(dailySummary1.hiChill); } }
        // Sensacion termica Min
        public double SensacionTermicaMin { get { return Fahr2Cels(dailySummary1.lowChill); } }
        // Humedad Max
        public double HumedadMax { get { return Convert.ToDouble(dailySummary1.hiOutHum) / 10.0; } }
        // Humedad Min
        public double HumedadMin { get { return Convert.ToDouble(dailySummary1.lowOutHum) / 10.0; } }
        // Humedad Promedio
        public double HumedadProm { get { return Convert.ToDouble(dailySummary1.avgOutHum) / 10.0; } }
        // Sensación térmica: Min
        public double MinSensacionTermica { get { return Fahr2Cels(dailySummary1.lowChill) / 10.0; } }
        // Sensación térmica: Max
        public double MaxSensacionTermica { get { return Fahr2Cels(dailySummary1.hiChill) / 10.0; } }
        // Viento: en km/hora
        public double VelViento { get { return Mph2KmHora(dailySummary1.avgSpeed); } }
        // Presion atmosférica Min en HPa
        public double PresionMin { get { return Hg2HPa(dailySummary1.lowBar); } }
        // Presion atmosférica Min en HPa
        public double PresionMax { get { return Hg2HPa(dailySummary1.hiBar); } }
        // Radiación solar total en w/m2
        public int RadiacionSolar { get { return Convert.ToInt32(dailySummary1.dailyUVDose); } }

        
        #endregion




        #region Conversion de unidades

        // Decimas de Fahrenheit a Celsius
        private Double Fahr2Cels(Single value)
        {
            return Math.Round((Convert.ToDouble(value) / 10.0 - 32.0) * 5.0 / 9.0, 1);
        }

        // Pulgadas de Mercuruio a HPa. (Hecto Pascal)
        // 1 inch of hg = 3386.39 Pa
        private Double Hg2HPa(Single value)
        {
            return Math.Round(Convert.ToDouble(value) * 0.0338639, 0);
        }


        private double Mph2KmHora(Single value)
        {
            return Math.Round(Convert.ToDouble(value) * 18.52, 1);
        }
        #endregion

    }

}
