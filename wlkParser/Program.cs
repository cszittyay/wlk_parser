﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace wlkPaser
{
    class Program
    {
        static void Main(string[] args)
        {
            var wlk = new WlkData();

            using (var reader = new BinaryReader(File.OpenRead(@"C:\VS2017\wlkParser\data\2018-03.wlk"))) 
            {
                wlk.Read(reader);
            }
            wlk.PrintData();
            Console.ReadKey();
        }
    }
}
